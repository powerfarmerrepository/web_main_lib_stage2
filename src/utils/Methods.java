package utils;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import main.Main;

public class Methods extends Main {

	
public static void clickOn(WebElement element){
	
	System.out.println(element.toString());
    wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    
 }

public static void clickOn(By by) {
	
	wait.until(ExpectedConditions.elementToBeClickable(by)).click();
	
}

public static void sendText(By by, String text)	{
	System.out.println("Send text to :  "+ by.toString()+ "   text    :    "+text);
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(by));
	element.clear();
	element.sendKeys(text);
	
}


public static void sendText(WebElement element, String text)	{
	System.out.println("Send text to :  "+ element.toString()+ "   text    :    "+text);
	wait.until(ExpectedConditions.elementToBeClickable(element));
	element.clear();
	element.sendKeys(text);
	
}

public static void switchToFrame(WebElement element)	{
	
	System.out.println(element.getClass().getName());
	wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
	
}


public static WebElement visible(WebElement element) {
	
    return  wait.until(ExpectedConditions.visibilityOf(element)); 
    
}

public static WebElement visible(By by) {
	
    return  wait.until(ExpectedConditions.visibilityOfElementLocated(by)); 
    
}

public static WebElement clickable(By by) {
	
    return  wait.until(ExpectedConditions.elementToBeClickable(by)); 
    
}



public static WebElement clickable(WebElement element) {
	
    return  wait.until(ExpectedConditions.elementToBeClickable(element)); 
    
}


public static boolean isVisible(By by) {
	
    try {
      
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return true;
       
    } catch (TimeoutException exception) {
    	
        return false;
    } 
    
    
}

public static void waitForInvisible(WebElement element, int timeout, WebDriver driver) {
	 
    WebDriverWait wait = new WebDriverWait(driver, timeout);
    wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));    
}



public static boolean isVisible(WebElement element) {
		
    try {
      
        wait.until(ExpectedConditions.visibilityOf(element));
        return true;
       
    } catch (TimeoutException exception) {
    	
        return false;
    } 
    
    
}

public static void scrollToAndClick(WebDriver driver, WebElement element){
	
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	jse.executeScript("arguments[0].scrollIntoView()", element); 
	element.click();	
	
}

public static String getDateTime(String format){
	
    return new SimpleDateFormat(format).format(new GregorianCalendar().getTime());	
  	
  }



}
