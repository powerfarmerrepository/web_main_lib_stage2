package main;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class PfUtils {
		

	
	/**
	 * Wykonanie zrzutu ekranu z przegladarki do pliku PNG.
	 * @param driver
	 */	
  public static String getWebScreenShot(WebDriver driver) throws IOException {
	 
	    WebDriver augmentedDriver = new Augmenter().augment(driver);
		File srcFile= ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
		String filePath =System.getProperty("user.dir")+ System.getProperty("file.separator")+"result"+ System.getProperty("file.separator")+"attachments"+ System.getProperty("file.separator");
		String fileName="Screenshot.png";
		FileUtils.copyFile(srcFile,new File(filePath+fileName));
		return fileName;  
		
  } 


	/**
	 * Wykonanie zrzutu ekranu z przegladarki do pliku PNG 
	 * przy wykorzystaniu biblioteki Ashot
	 * @param driver
	 */	
	public static void getAShot(WebDriver driver) {
		
		 Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(3000)).takeScreenshot(driver);
		 
		 try {
			ImageIO.write(fpScreenshot.getImage(),"PNG",new File("Screenshot.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
    
	
	/**
	 * Walidacja istnienia pliku.
	 * @param path bezwgledna sciezka wraz z nazwa pliku i rozszerzeniem
	 * @return <code>true</code> = plik istnieje
	 */
	public static boolean fileExist(String path){
		
		boolean returnValue = false;
		Path oPath = Paths.get(path);
		if (Files.exists(oPath)) {
			returnValue = true;
		} 

		return returnValue;
		
	}
	
   
	public static List<File> search(File root, String name) {
	    List<File> found = new ArrayList<File>();
	    if (root.isFile() && root.getName().equalsIgnoreCase(name)) {
	        found.add(root);
	        System.out.println(root.getAbsolutePath());
	    } else if (root.isDirectory()) {
	        for (File file : root.listFiles()) {
	            found.addAll(search(file, name));
	        }
	    }
	    return found;
	}	
		
	 public static void findFile(String name,File file)
	    {
		 
		    System.out.println(file.getAbsolutePath());
	        File[] list = file.listFiles();
	        if(list!=null)
	        for (File fil : list)
	        {
	            if (fil.isDirectory())
	            {
	                findFile(name,file);
	            }
	            else if (name.equalsIgnoreCase(fil.getName()))
	            {
	                System.out.println(fil.getParentFile());
	            }
	        }
	    }
	

	 
	 /*=============================================================================================
		CAPABILITIES
	=============================================================================================*/	 
	 
	 
public DesiredCapabilities setAdditionalMobileCapabilities(String os,DesiredCapabilities cap) throws IOException {
	
	Map<String,String> capMap = new HashMap<String,String>();
	os = os.toLowerCase();
	
	switch (os) {
	
	case "ios":
	  
		capMap = readProperties("c-ios.cap",capMap);
		capMap = readProperties("t-ios.cap",capMap);
	       cap = addCapabilities(cap, capMap);
	  break;
		
	case "android":
      
		capMap = readProperties("c-android.cap",capMap);
		capMap = readProperties("t-android.cap",capMap);
		   cap =  addCapabilities(cap, capMap);      
	  break;	
				
	default:
		break;
	}
	
	
	return cap; 
	
		
	}



public DesiredCapabilities setAdditionalWebCapabilitiesOptions(String browser,DesiredCapabilities cap) throws IOException {
	
	Map<String,String> capMap = new HashMap<String,String>();
	browser = browser.toLowerCase();
	
	switch (browser) {
	
	case "chrome":
	  
		capMap = readProperties("c-chrome.cap",capMap);
		capMap = readProperties("t-chrome.cap",capMap);
		capMap = readProperties("c-chrome.opt",capMap);
		capMap = readProperties("t-chrome.opt",capMap);
		
	    cap = addCapabilities(cap, capMap);
	  break;
		
	case "firefox":
      
		capMap = readProperties("c-firefox.cap",capMap);
		capMap = readProperties("t-firefox.cap",capMap);
		capMap = readProperties("c-firefox.opt",capMap);
		capMap = readProperties("t-firefox.opt",capMap);
		
		   cap =  addCapabilities(cap, capMap);      
	  break;	
	  
	case "ie":
	      
		capMap = readProperties("c-ie.cap",capMap);
		capMap = readProperties("t-ie.cap",capMap);
		capMap = readProperties("c-ie.opt",capMap);
		capMap = readProperties("t-ie.opt",capMap);
		
		   cap =  addCapabilities(cap, capMap);      
	  break;	 
	  
				
	default:
		break;
	}
	
	
	return cap; 
	
		
	}





public DesiredCapabilities addCapabilities(DesiredCapabilities cap,Map<String,String> capMap) {
	
	for (Map.Entry<String, String> entry : capMap.entrySet())
	{
	    System.out.println(entry.getKey() + "/" + entry.getValue());
	    cap.setCapability(entry.getKey(), entry.getValue());
	}
	
	return cap;
	
		
}
	
public Map<String, String> readProperties(String propertiesFile, Map<String, String> capMap) {
		
	try {
				
		File file = new File(propertiesFile);
		FileInputStream fileInput = new FileInputStream(file);
		Properties properties = new Properties();
		properties.load(fileInput);
		fileInput.close();
        
		Enumeration<?> enuKeys = properties.keys();
		while (enuKeys.hasMoreElements()) {
			String key = (String) enuKeys.nextElement();
			String value = properties.getProperty(key);
			System.out.println(key + ": " + value);
			capMap.put(key, value);
		}
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	return capMap;
	
	
	
}	
	
/*
public static String getMobileScreenShot(AppiumDriver driver) throws IOException {
	
	File  srcFiler=driver.getScreenshotAs(OutputType.FILE);
	String filePath =System.getProperty("user.dir")+ System.getProperty("file.separator")+"result"+ System.getProperty("file.separator")+"attachments"+ System.getProperty("file.separator");
	String fileName="Screenshot.png";
	FileUtils.copyFile(srcFiler,new File(filePath+fileName));
	return fileName;
} */





}
		
