package main;
import org.testng.ITestResult;
import java.io.IOException;

import org.json.JSONException;
import org.testng.IConfigurationListener2;


public class ConfigurationListener implements IConfigurationListener2 {

	@Override
	public void onConfigurationSuccess(ITestResult itr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConfigurationFailure(ITestResult itr) {
	
		try {
			
			Main.parameters.setResult("failed", "Configuration error", itr.getThrowable().toString(), "", "");
			Main.report.logFail(itr.getThrowable().toString());
			
		} catch (JSONException e) {
			
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} 
		
		
	}

	@Override
	public void onConfigurationSkip(ITestResult itr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeConfiguration(ITestResult tr) {
		
		
	}

}