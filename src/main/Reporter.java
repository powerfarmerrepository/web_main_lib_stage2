package main;

import java.io.BufferedWriter;
import java.io.FileWriter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import utils.Methods;


/**
 * Raportowanie customowe przebiegu Uslugi.
 */
public class Reporter {

	public ExtentReports extent;
	public ExtentTest reporter;
	
	private final String fileName;
	private boolean loggingOn;				// flaga: czy wylaczyc logowanie - domyslnie true
	private boolean logFailAndErrorOnly;	// flaga: czy logowac tylko statusy  FAIL i ERROR  - domyslnie false
	

	/** 
	 * KONSTRUKTOR
	 * @param reportPathFileName 
	 * 		sciezka z nazwa, gdzie zapisac plik raportu
	 */
	public Reporter(String reportPathFileName) {
		this.extent = new ExtentReports(reportPathFileName);
//		this.extent = new ExtentReports(reportPathFileName, NetworkMode.OFFLINE);
		loggingOn = true;
		logFailAndErrorOnly = false;
		fileName = (new StringBuilder())
			.append(System.getProperty("user.dir"))
			.append(System.getProperty("file.separator"))
			.append("result")
			.append(System.getProperty("file.separator"))
			.append("attachments")
			.append(System.getProperty("file.separator"))
			.append("service.log").toString();
	}
	
	
	@Override
	public String toString() {
		return (new StringBuilder())
			.append(loggingOn ? "logowanie ON" : "logowanie OFF")
			.append(!loggingOn ? "" : (logFailAndErrorOnly ? ", logowanie statusow INFO, FAIL i ERROR" : ", logowanie wszystkich statusow"))
			.toString();
	}
	
	/**
	 * Utworzenie nowego wpisu w strukturze raportowej.
	 * @param testName
	 * 		nazwa testu
	 */
	public void startTest(String testName) {
		reporter = this.extent.startTest(testName);
	}
	
	
	/**
	 * Dodanie wpisu informacyjnego (INFO).
	 * @param msg
	 * 		tresc wpisu
	 */
	public void logInfo(String msg) {

		if (loggingOn && !logFailAndErrorOnly) {
		reporter.log(LogStatus.INFO, msg);
		writeTechnicalLog("[INFO] :  " + msg);	
		}
	}
	
	
	/**
	 * Dodanie statusu pozytywnego (PASS).<br/>
	 * Sterowanie czy dodac wpis za pomoca flag.
	 * @param msg
	 * 		tresc wpisu
	 */
	public void logPass(String msg) {
		
		if (loggingOn && !logFailAndErrorOnly) {
			reporter.log(LogStatus.PASS, msg);
			writeTechnicalLog("[PASS] :  " + msg);
		}
	}
	
	
	/**
	 * Dodanie statusu negatywnego (FAIL).<br/>
	 * Sterowanie czy dodac wpis za pomoca flag.
	 * @param msg
	 * 		tresc wpisu
	 */
	public void logFail(String msg) {
		if (loggingOn) {
			reporter.log(LogStatus.FAIL, msg);
			writeTechnicalLog("[FAIL] :  " + msg);
		}
	}
	
	
	/**
	 * Dodanie statusu bledu (FAIL).<br/>
	 * Sterowanie czy dodac wpis za pomoca flag.
	 * @param msg
	 * 		tresc wpisu
	 */
	public void logError(String msg) {
		if (loggingOn) {
			reporter.log(LogStatus.ERROR, msg);
			writeTechnicalLog("[ERROR] :  " + msg);
		}
	}
	
	
	/**
	 * Sterowanie czy wlaczyc logowanie. Beda dodwane tylko statusy <pre>INFO</pre>.
	 */
	public void loggingOn() { this.loggingOn = true; }
	
	
	/**
	 * Sterowanie czy wylaczyc cale logowanie. Beda dodawane tylko statusy <pre>INFO</pre>.
	 */
	public void loggingOff() { this.loggingOn = false; }
	
	
	/**
	 * Sterowanie czy wlaczyc logowanie tylko dla statusow <pre>INFO, FAIL, ERROR</pre>.
	 */
	public void logFailsErrorsOn() { this.logFailAndErrorOnly = true; }
	
	
	/**
	 * Sterowanie czy wylaczyc logowanie tylko dla statusow <pre>INFO, FAIL, ERROR</pre>.
	 */
	public void logFailsErrorsOff() { this.logFailAndErrorOnly = false; }
	
	
	/**
	 * Dodanie zrzutu ekranowego do raportu.
	 * @param imagePath
	 * 		sciezka z nazwa pliku ze zrzutem ekranowym
	 */
	public void logScreenShot(String imagePath) {
		reporter.addScreenCapture(imagePath);
	}
	
	
	/**
	 * Wpis do logu technicznego
	 * @param str
	 * 		tresc wpisu
	 */
	public void writeTechnicalLog(String str) {
		BufferedWriter writer ;
		try {
			writer = new BufferedWriter(new FileWriter(fileName, true));
			writer.append(Methods.getDateTime("yyyy-MM-dd HH:mm:ss  ") + str);
			writer.newLine();
			writer.close();
		} catch (Exception e) {}
		
		finally { }
	}
	
	
	/**
	 * Wygenerowanie pliku raportu.
	 */
	public void generateRaport() {
		extent.endTest(reporter);
		extent.flush();
		extent.close();
	}
	
}
